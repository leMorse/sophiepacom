<!DOCTYPE html>
<html class="no-js" dir="ltr" lang="<?php echo Theme::lang() ?>">

  <head>
	  <meta charset="utf-8" />
	  <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php echo Theme::metaTagTitle(); ?>

    <script>
      if (document.documentElement) {
	      document.documentElement.className = 'js';
      }
    </script>

    <?php echo Theme::css('css/top.min.css') ?>

    <meta name="robots" content="index, follow" />
    <meta name="author" content="le Morse" />
    <meta name="generator" content="Bludit" />

    <link href="/humans.txt" rel="author" type="text/plain" />
    <link href="/favicon.ico" rel="icon" type="image/x-icon" />

    <?php Theme::plugins('siteHead') ?>
  </head>

  <?php
    if ($WHERE_AM_I == 'page' && $page->template()) {
      $tpl_string = $page->template();
    }
  ?>

  <body class="<?php echo $WHERE_AM_I; ?><?php echo isset($tpl_string) ? ' '.$tpl_string : ''; ?>">
	  <?php Theme::plugins('siteBodyBegin'); ?>

	  <header class="header-banner">
      <div class="logo"><?php echo $site->title(); ?></div>
      <?php include(THEME_DIR_PHP . 'navbar.php'); ?>
    </header>

	  <main
      class="main<?php echo isset($tpl_string) ? ' main--'.$tpl_string : ''; ?>"
      id="m">
      <?php
	      // $WHERE_AM_I variable detect where the user is browsing
	      // If the user is watching a particular page the variable takes the value "page"
	      // If the user is watching the frontpage the variable takes the value "home"
	      if ($WHERE_AM_I == 'page') {
          if ($page->template()) {
		        include(THEME_DIR_PHP.$page->template().'.php');
	        } else {
		        include(THEME_DIR_PHP.'page.php');
	        }
	      } else {
		      include(THEME_DIR_PHP . 'home.php');
	      }
	    ?>
    </main>

    <?php echo Theme::css('css/bottom.min.css') ?>
	  <?php include(THEME_DIR_PHP . 'footer.php'); ?>
	  <?php Theme::plugins('siteBodyEnd'); ?>
    <?php echo Theme::js('js/bottom.min.js') ?>
  </body>
</html>
