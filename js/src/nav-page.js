(function () {
  'use strict';
  var navPage = document.getElementById('navPage');

  if (navPage) {
    var relnext = navPage.querySelector('[rel="next"]');
    var relprev = navPage.querySelector('[rel="prev"]');

    // Navigate through portfolio's pages with left and right arrows
    document.addEventListener('keydown', function(evt) {
      if (evt.keyCode == '37' && relprev) {
        relprev.click();
      } else if (evt.keyCode == '39' && relnext) {
        relnext.click();
      }

      return;
    });
  }
})();
