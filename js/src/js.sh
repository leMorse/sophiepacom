#!/bin/sh

# You can use it with entr to rebuild on change:
# find . | entr -d ./js.sh

# Dependencies:
# - jsmin (clang)

dst="../"

# Bottom bundle
# Return bottom.min.css in $dst
while read -r line; do
  [ -n "$line" ] && cat "./$line"
done <<-EOF | jsmin > "${dst}bottom.min.js"
nav-page.js

EOF



echo "JS builded! @ $(date +"%Y-%m-%d %T")"
