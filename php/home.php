<?php if (empty($content)) : ?>
  <?php $language->p('No pages found') ?>
<?php endif ?>

<!-- Load Bludit Plugins: Page Begin -->
<?php Theme::plugins('pageBegin'); ?>
<?php echo Theme::css('css/home.min.css') ?>

<ol class="home-grid" reversed="" role="list">
  <?php foreach ($content as $page) : ?>
    <li>
      <h2 class="visuallyhidden"><?php echo $page->title(); ?></h2>
      <a href="<?php echo $page->permalink(); ?>">
        <img
          alt=""
          height="400"
          loading="lazy"
          src="<?php echo $page->thumbCoverImage(); ?>"
          width="400" />
      </a>
    </li>
  <?php endforeach ?>
</ol>

<!-- Load Bludit Plugins: Page End -->
<?php Theme::plugins('pageEnd'); ?>
