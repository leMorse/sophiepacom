<nav class="nav" id="n" aria-label="Navigation principale">
  <ul role="list">
    <li>
      <a href="<?php echo Theme::siteUrl(); ?>" rel="index">Accueil</a>
    </li>

    <?php foreach ($staticContent as $staticPage): ?>
      <?php if (!$staticPage->isChild()): ?>
	      <li>
		      <a href="<?php echo $staticPage->permalink(); ?>">
            <?php echo $staticPage->title(); ?>
          </a>
	      </li>
      <?php endif ?>
    <?php endforeach ?>
  </ul>
</nav>
