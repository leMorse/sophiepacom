<?php Theme::plugins('pageBegin'); ?>
<?php echo Theme::css('css/portfolio.min.css') ?>

<?php
	$nextPageKey = $page->nextKey();
  $previousPageKey = $page->previousKey();
	$nextPage = buildPage($nextPageKey);
	$previousPage = buildPage($previousPageKey);
?>

<?php if ($page->isChild()): ?>
  <!-- <a class="go-back" href="<?php echo $page->parentMethod('permalink'); ?>">
       &larr; <?php echo $page->parentMethod('title'); ?>
       </a> -->
<?php endif ?>

<article>
  <?php echo $page->content(); ?>

  <figure class="portfolio__fig">
    <?php if ($page->coverImage()) : ?>
      <img alt="" src="<?php echo $page->coverImage(); ?>" />
    <?php endif ?>
    <figcaption>
      <h1><?php echo $page->title(); ?></h1>
    </figcaption>
  </figure>

  <?php echo Theme::css('css/nav-page.min.css') ?>

  <nav aria-label="Navigation séquentielle par page" class="nav-page" id="navPage">
    <?php if ($nextPage->title()): ?>
      <a href="<?php echo $nextPage->permalink() ;?>" rel="next">
        &larr;<span class="visuallyhidden">&nbsp;Suivante:<?php echo $nextPage->title() ;?></span>
      </a>
    <?php endif ?>

    <?php if ($previousPage->title()): ?>
      <a href="<?php echo $previousPage->permalink() ;?>" rel="prev">
        <span class="visuallyhidden">Précédente:<?php echo $previousPage->title() ;?>&nbsp;</span>&rarr;
      </a>
    <?php endif ?>
  </nav>
</article>


<?php Theme::plugins('pageEnd'); ?>
