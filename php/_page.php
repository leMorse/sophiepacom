<?php
  /* Load Bludit Plugins: Page Begin */
  Theme::plugins('pageBegin');
?>

<!-- Page title -->
<h1 class="title"><?php echo $page->title(); ?></h1>


<?php if (!$page->isStatic() && !$url->notFound() && $themePlugin->showPostInformation()) : ?>
  <div class="form-text mb-2">
    <!-- Page creation time -->
    <span class="pr-3"><i class="bi bi-calendar"></i><?php echo $page->date() ?></span>

    <!-- Page reading time -->
    <span class="pr-3"><i class="bi bi-clock"></i><?php echo $page->readingTime() . ' ' . $L->get('minutes') . ' ' . $L->g('read') ?></span>

    <!-- Page author -->
    <span><i class="bi bi-person"></i><?php echo $page->user('nickname') ?></span>
	</div>
<?php endif ?>

dlskjdlkjslkdlk

<?php if ($page->description()) : ?>
	<p><?php echo $page->description(); ?></p>
<?php endif ?>

<?php if ($page->coverImage()) : ?>
  <img alt="" src="<?php echo $page->coverImage(); ?>" />
<?php endif ?>

<div class="page-content">
  <?php echo $page->content(); ?>
</div>

<?php
  /* Load Bludit Plugins: Page End */
  Theme::plugins('pageEnd');
?>
