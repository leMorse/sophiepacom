<?php /* Load Bludit Plugins: Page Begin */ ?>
<?php Theme::plugins('pageBegin'); ?>

<?php if ($page->isChild()): ?>
  <!-- <a class="go-back" href="<?php echo $page->parentMethod('permalink'); ?>">
       &larr; <?php echo $page->parentMethod('title'); ?>
       </a> -->
<?php endif ?>

<article>
  <?php /* Page title */ ?>
  <h1 class="title"><?php echo $page->title(); ?></h1>

  <?php /*Page content*/ ?>
  <?php echo $page->content(); ?>

  <?php if ($page->coverImage()) : ?>
    <img alt="" src="<?php echo $page->coverImage(); ?>" />
  <?php endif ?>
</article>

<?php /*Load Bludit Plugins: Page End */ ?>
<?php Theme::plugins('pageEnd'); ?>
