#!/bin/sh
# You can use it with entr to rebuild on change:
# find . | entr -d ./css.sh

# Dependencies:
# - cssmin (lua)

# Destination folder
dst="../"

# Top bundle
# Return top.min.css in $dst
while read -r line; do
  [ -n "$line" ] && cat "./$line"
done <<-EOF | cssmin > "${dst}top.min.css"
root.css
base.css
header-banner.css
logo.css
nav.css
main.css

EOF

# Bottom bundle
# Return bottom.min.css in $dst
while read -r line; do
  [ -n "$line" ] && cat "./$line"
done <<-EOF | cssmin > "${dst}bottom.min.css"
utilities.css
footer-banner.css

EOF

# Modular
cssmin home.css > "${dst}home.min.css"
cssmin portfolio.css > "${dst}portfolio.min.css"
cssmin nav-page.css > "${dst}nav-page.min.css"

echo "CSS builded! @ $(date +"%Y-%m-%d %T")"
